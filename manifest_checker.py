import os
import json
from collections import defaultdict

# Function to check if a folder path is valid
def is_valid_folder(path):
    return os.path.isdir(path)

# Function to open manifest.json and retrieve required values
def get_manifest_info(folder_path):
    manifest_path = os.path.join(folder_path, 'manifest.json')
    if os.path.exists(manifest_path):
        with open(manifest_path, 'r') as manifest_file:
            manifest_data = json.load(manifest_file)
            name = manifest_data.get('name', 'N/A')
            short_name = manifest_data.get('short_name', 'N/A')
            default_title = manifest_data.get('action', {}).get('default_title', 'N/A')
            version = manifest_data.get('version', 'N/A')
            manifest_version = manifest_data.get('manifest_version', -1)  # Get manifest_version
            return name, short_name, default_title, version, manifest_version
    return 'N/A', 'N/A', 'N/A', 'N/A', -1

# Ask user for folder path
folder_path = input("Enter the profile path: ")

# Check if the entered folder path is valid
if is_valid_folder(folder_path):
    extensions_path = os.path.join(folder_path, 'Extensions')

    # Check if Extensions folder exists
    if os.path.exists(extensions_path) and os.path.isdir(extensions_path):
        extensions_list = []

        # Iterate through each folder in Extensions folder
        for root, dirs, files in os.walk(extensions_path):
            for folder in dirs:
                folder_name = os.path.join(root, folder)
                manifest_path = os.path.join(folder_name, 'manifest.json')
                if os.path.exists(manifest_path):
                    extensions_list.append(folder_name)
        
        # Sort the array by folder name
        extensions_list.sort()

        # Gather manifest info and store in the array
        manifest_info = defaultdict(list)
        for extension_folder in extensions_list:
            name, short_name, default_title, version, manifest_version = get_manifest_info(extension_folder)
            parent_folder = os.path.split(os.path.dirname(extension_folder))[1]  # Get the parent directory name
            manifest_info[manifest_version].append({'name': name, 'short_name': short_name, 'default_title': default_title, 'version': version, 'folder_name': parent_folder})

        # Sort the keys (manifest versions) in descending order
        sorted_versions = sorted(manifest_info.keys(), reverse=True)

        total_count = 0  # To store the total count across all groups

        # Print the array grouped by manifest_version and count for each group
        for version in sorted_versions:
            group_count = len(manifest_info[version])
            total_count += group_count
            print(f"Manifest version {version} ({group_count}):")
            for item in manifest_info[version]:
                short_name = item['short_name']
                default_title = item['default_title']
                name = item['name']
                formatted_name = name
                if short_name != name and short_name != 'N/A' and short_name is not None:
                    formatted_name += f" ({short_name}"
                    if default_title != name and default_title != 'N/A' and default_title is not None:
                        formatted_name += f", {default_title})"
                    else:
                        formatted_name += ")"
                else:
                    if default_title != name and default_title != 'N/A' and default_title is not None:
                        formatted_name += f" ({default_title})"
                print(f"{formatted_name} | {item['version']} | {item['folder_name']}")
            print()  # Print an empty line between different manifest versions

        # Print the total count across all groups
        print(f"Total: {total_count}")

    else:
        print("Extensions folder not found in the provided path.")
else:
    print("Invalid folder path entered.")
