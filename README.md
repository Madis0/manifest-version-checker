# Manifest version checker

This Python 3 script checks your browser extensions to see how many of them have already upgraded to Manifest v3.

## How to use (Chromium)

This tutorial is for you if you're using a Chromium-based browser:

* Google Chrome
* Google Chromium
* Microsoft Edge
* Opera
* Brave browser
* Vivaldi browser
* Yandex browser
* Ungoogled Chromium
* _and so on..._

1. [Download latest Python for your OS](https://www.python.org/downloads/) (if you don't already have it)
2. [Download this script](https://gitlab.com/Madis0/manifest-version-checker/-/raw/main/manifest_checker.py?ref_type=heads&inline=false)
3. Open `about:version` or `chrome://version` in your browser
4. Copy the **Profile path**
5. Run the script, e.g. from a terminal `python manifest_checker.py`
6. Read the output. Some extensions may have weird names like `__MSG_extName__` - in that case just search the last element (e.g. _cjpalhdlnb..._) online to know what it refers to.

## How to use (Firefox)

This tutorial is for you if you're using a Firefox-based browser:

* Mozilla Firefox
* LibreWolf
* WaterFox
* Tor Browser
* ~~Pale Moon~~ (uses different extension format anyway)
* ~~Basilisk Browser~~ (uses different extension format anyway)
* Floorp
* Mercury Browser
* _and so on..._

1. [Download latest Python for your OS](https://www.python.org/downloads/) (if you don't already have it)
2. [Download this script](https://gitlab.com/Madis0/manifest-version-checker/-/raw/main/firefox_manifest_checker.py?ref_type=heads&inline=false)
3. Open `about:profiles` in your browser
4. Copy the **Root Directory**
5. Run the script, e.g. from a terminal `python firefox_manifest_checker.py`
6. Read the output. Some extensions may have weird names like `__MSG_extensionName__` - in that case just search the last element (e.g. _{6fa42eda-...}_) online to know what it refers to.

## What is this?

"Manifest version" refers to the type of technology standard that your browser extensions use. Most extensions are on version 2 right now, but browsers already support 3 as well. Now Google has decided that [version 2 should be deprecated and all extensions should move to version 3](https://developer.chrome.com/blog/resuming-the-transition-to-mv3/#the-phase-out-timeline). Other browsers will follow suit, although with a potentially different deadline.

## Why should I care?

"Manifest V3" is a keyword you might've seen in the news, alongside with "Chrome banning adblockers". The reality is that adblockers were not planned to be _disabled_, just _less effective_ in blocking all kinds of ads, on all sites. The latest updates to the standard have claimed to fix that, though final results (of what actually changes for you) remain to be seen when the extensions update to the new standard.

Either way, you should consider that any other extensions are affected by this as well, so think about: Do you have any extensions that haven't been updated in a while? Do you still need them? Can you replace them if needed? Would you adapt if they lost or changed functionality?

## When should I care?

[June 2024](https://developer.chrome.com/blog/resuming-the-transition-to-mv3/#the-phase-out-timeline) is when the transition will begin, so during or before that.

## What are my other options?

If you can't live without those extensions or find replacements in time, consider switching your browser instead. [Mozilla Firefox](https://www.mozilla.org/en-US/firefox/new/), [Vivaldi Browser](https://vivaldi.com/) and [Brave Browser](https://brave.com/) are good choices that promised to extend the timeline and they have more functionality built-in, which may also serve your use case, especially when it comes to [ad blocking](https://ublockorigin.com/).
