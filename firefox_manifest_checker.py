import os
import json
from zipfile import ZipFile
from collections import defaultdict

# Function to check if a folder path is valid
def is_valid_folder(path):
    return os.path.isdir(path)

# Function to extract and read manifest.json from xpi file
def extract_manifest_info(file_path):
    try:
        with ZipFile(file_path, 'r') as zfile:
            manifest_content = zfile.read('manifest.json')
            manifest_data = json.loads(manifest_content)
            return manifest_data
    except Exception as e:
        print(f"Error extracting manifest.json from {file_path}: {e}")
        return None

# Ask user for folder path
folder_path = input("Enter the profile path: ")

# Check if the entered folder path is valid
if is_valid_folder(folder_path):
    extensions_path = os.path.join(folder_path, 'extensions')

    # Check if extensions folder exists
    if os.path.exists(extensions_path) and os.path.isdir(extensions_path):
        extensions_list = []

        # Iterate through each file in extensions folder
        for file in os.listdir(extensions_path):
            if file.endswith('.xpi'):
                xpi_path = os.path.join(extensions_path, file)
                folder_name = os.path.splitext(file)[0]  # Extract xyz from {xyz}.xpi
                extensions_list.append({'xpi_path': xpi_path, 'folder_name': folder_name})

        # Group extensions by manifest_version
        manifest_info = defaultdict(list)
        for extension in extensions_list:
            manifest_data = extract_manifest_info(extension['xpi_path'])
            if manifest_data:
                manifest_version = manifest_data.get('manifest_version', 'N/A')
                name = manifest_data.get('name', 'N/A')
                short_name = manifest_data.get('short_name', 'N/A')
                default_title = manifest_data.get('action', {}).get('default_title', 'N/A')
                version = manifest_data.get('version', 'N/A')
                manifest_info[manifest_version].append({
                    'name': name,
                    'short_name': short_name,
                    'default_title': default_title,
                    'version': version,
                    'folder_name': extension['folder_name']
                })

        # Sort manifest_versions in descending order
        sorted_versions = sorted(manifest_info.keys(), reverse=True)

        total_count = sum(len(ext) for ext in manifest_info.values())  # Total count of extensions

        # Print the array grouped by manifest_version
        for version in sorted_versions:
            extensions = manifest_info[version]
            print(f"Manifest version {version} ({len(extensions)}):")
            for item in extensions:
                name = item['name']
                short_name = item['short_name']
                default_title = item['default_title']
                formatted_name = name
                if short_name != name and short_name != 'N/A':
                    formatted_name += f" ({short_name}"
                    if default_title != name and default_title != 'N/A':
                        formatted_name += f", {default_title})"
                    else:
                        formatted_name += ")"
                else:
                    if default_title != name and default_title != 'N/A':
                        formatted_name += f" ({default_title})"
                print(f"{formatted_name} | {item['version']} | {item['folder_name']}")
            print()  # Empty line between different manifest versions
        
        # Print the total count of extensions
        print(f"Total: {total_count}")

    else:
        print("Extensions folder not found in the provided path.")
else:
    print("Invalid folder path entered.")
